module.exports = function(grunt) {
  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    sass: {
      // Client
      client: {
        options: {
          style: 'expanded',
          sourcemap: "none"
        },
        files: {
          'webroot/css/style.css': 'webroot/sass/styles.sass'
        }
      },
      //maintenance
      maintenance: {
        options: {
          style: 'expanded',
          sourcemap: "none"
        },
        files: {
          'webroot/common/maintenance/css/styles.css': 'webroot/common/maintenance/sass/styles.sass'
        }
      },
      // Admin
      admin: {
        options: {
          style: 'expanded',
          sourcemap: "none"
        },
        files: {
          'webroot/admin-assets/css/admin_styles.css': 'webroot/admin-assets/sass/admin_styles.sass'
        }
      }
    },
    cssmin: {
      // Client
      client: {
        options: {
          shorthandCompacting: false,
          roundingPrecision: 8,
          rebase: false
        },
        files: {
          'webroot/css/final.css': [
            "webroot/css/init/client/*",
            'webroot/admin-assets/css/select2.css',
            "webroot/css/style.css"
          ]
        }
      },
      // Admin
      admin: {
        options: {
          shorthandCompacting: false,
          roundingPrecision: -1,
          rebase: false
        },
        files: {
          'webroot/admin-assets/css/final.css': [
            'webroot/admin-assets/css/bootstrap-datetimepicker.css',
            "webroot/common/css/icomoon/style.css",
            'webroot/admin-assets/css/bootstrap3.css',
            'webroot/admin-assets/css/bootstrap-tagsinput.css',
            'webroot/admin-assets/css/select2.css',
            'webroot/admin-assets/css/admin_styles.css'
          ]
        }
      }
    },
    uglify: {
      options: {
        mangle: false
      },
      // Client
      client: {
        files: {
          'webroot/js/final.js': [
            'webroot/js/init/*.js',

          ]
        }
      },
      // Admin
      admin: {
        files: {
          // Destination: Origin
          'webroot/admin-assets/js/final.js': [
            'webroot/admin-assets/js/moment.js',
            'webroot/admin-assets/js/bootstrap-datetimepicker.js',
            'webroot/admin-assets/js/ro.js',
            'webroot/admin-assets/js/bootstrap-tagsinput.js',
            'webroot/admin-assets/js/bootstrap.js',
            'webroot/admin-assets/js/jquery-ui.js',
            'webroot/admin-assets/js/moment.js',
            'webroot/admin-assets/js/jquery.nestedSortable.js',
            'webroot/common/js/autosize.js',
            'webroot/common/js/select2.js',
            'webroot/admin-assets/js/url.min.js',
            'webroot/admin-assets/js/admin.js'
          ]
        }
      }
    },
    watch: {
      clientcss: {
        files: ['webroot/sass/*','webroot/sass/partials/*'],
        tasks: ['sass:client', 'cssmin:client']
      },
      admincss:{
        files: [
          'webroot/admin-assets/css/*',
          'webroot/admin-assets/sass/admin_styles.sass',
          '!webroot/admin-assets/css/final.css'
        ],
        tasks: ['sass:admin', 'cssmin:admin']
      },
      adminjs:{
        files: [
          'webroot/admin-assets/js/*',
          '!webroot/admin-assets/js/final.js',
        ],
        tasks: ['uglify:admin']
      }
    },
    // Available Tasks
    availabletasks: {
      tasks: {
        options: {
          tasks: [ 'client-css', 'client-js', 'admin-css',  'admin-js', 'client', 'admin', 'final'],
          sort: false,
          groups: {
            'User divided optimization': ['admin', 'client'],
            'Extension divided optimization': ['client-css', 'client-js', 'admin-css', 'admin-js'],
            'Complete optimization': 'final'
          },
          descriptions: {
            'admin': "Optimize Administrator's JavaScript and CSS files.",
            'client': "Optimize Client's JavaScript and CSS files.",
            'final': 'Optimize JavaScript and CSS files.'
          }
        }
      }
    }
  });

  // Load modules
  grunt.loadNpmTasks('grunt-available-tasks');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Register tasks
  grunt.registerTask('help', 'availabletasks');

  grunt.registerTask('watch-client', ['watch:client','watch:client']);

  // Admin js
  grunt.registerTask('admin-js', ['uglify:admin']);

  // Client js
  grunt.registerTask('client-js', ['uglify:client']);

  // Admin css
  grunt.registerTask('admin-css', ['sass:admin', 'cssmin:admin']);

  // Client css
  grunt.registerTask('client-css', ['sass:client', 'cssmin:client']);

  // Maintenance css
  grunt.registerTask('client-maintenance', ['sass:maintenance']);

  // Admin
  grunt.registerTask('admin', ['admin-js','admin-css']);
  // Client
  grunt.registerTask('client', ['client-js', 'client-css']);

  // Final
  grunt.registerTask('final', ['sass', 'cssmin', 'uglify']);
};
