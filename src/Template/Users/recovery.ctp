<form id="recovery-password-form" >
	<h1><?php echo conf('Config.projectName-eng') ?></h1>
	<div class="col-block">
		<label>Username</label>
		<input class="form-control" type="text" name="username" >
	</div>
	<div class="col-block">
		<input value="Send" class="btn btn-medium btn-block btn-submit btn-green" type="submit">

		<div class="message-block" style="display: none;" ></div>

		<a class="btn btn-grey-empty btn-block" href="/admin/users/login">Login form?</a>
	</div>
</form>

<script>
	var $formId = $("form#recovery-password-form");
	var $msgBlock = $('.message-block');
	var $loginBlock = $('.login-block');

	$formId.submit(function(e) {
		e.preventDefault();
		$loginBlock.removeClass('shake');
		$.ajax({
			type: "POST",
			data: $(this).serialize(),
			dataType: "json",
			success: function(response) {
				if (response) {
					if (response.class == 'error') {
						$loginBlock.addClass('shake');
					}

					$msgBlock.addClass(response.class).text(response.msg).fadeIn(500);

					if (response.class == 'success') {
						$formId[0].reset();
						$msgBlock.fadeOut(1500, function() {
							$msgBlock.removeClass('success error');
						});
					}

				}

			}
		});
	});
</script>