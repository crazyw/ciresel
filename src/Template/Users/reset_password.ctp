<form id="reset-password-form" >
    <h1><?php echo conf('Config.projectName-eng') ?></h1>
    <div class="col-block">
        <label>Password</label>
        <input class="form-control" type="password" name="password" >
        <label>Confirm password</label>
        <input class="form-control" type="password" name="confirm_password" >
    </div>
    <div class="col-block">
        <input value="Send" class="btn btn-medium btn-block btn-submit btn-green" type="submit">

        <div class="message-block" style="display: none;" ></div>
    </div>
</form>
<script>
    var $formId = $("form#reset-password-form");
    var $msgBlock = $('.message-block');
    var $loginBlock = $('.login-block');

    $formId.submit(function(e) {
        e.preventDefault();
        $loginBlock.removeClass('shake');
        $.ajax({
            type: "POST",
            data: $(this).serialize(),
            dataType: "json",
            success: function(response) {
                if (response) {
                    if (response.class == 'error') {
                        $loginBlock.addClass('shake');
                    }

                    $msgBlock.addClass(response.class).text(response.msg).fadeIn(500);

                    if (response.class == 'success') {
                        setTimeout(function () {
                            window.location.href = window.location.origin + '/admin/users/login';
                        }, 1000);
                    }

                }

            }
        });
    });
</script>