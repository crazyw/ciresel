<?php
echo $this->element('header', ['itemPage' => true,'langSwitcher'=>true]);

echo $this->Html->div('content-block',
	$this->Form->create($page, ['class' => "form-page"])
	.
	$this->Form->control('id',['type' => 'hidden'])
	.
	$this->Html->div('row',
		$this->Form->control('name',[
			'lang'=>true,
			'label'=>false,
			'templates' => ['inputContainer' => '{{content}}'],
			'templateVars' => [
				'label'=>__da('Name'),
				'class' => 'col-sm-12',
			]
		])
	)
    .
    $this->Html->div('row',
        $this->Form->control('workplace',[
            'lang'=>true,
            'label'=>false,
            'templates' => ['inputContainer' => '{{content}}'],
            'templateVars' => [
                'label'=>__da('Work place'),
                'class' => 'col-sm-12',
            ]
        ])
    )
	.
	$this->Html->div('row',
		$this->Form->control('content',[
			'lang'=>true,
			'type' => 'textarea',
			'label'=>false,
			'templates' => ['inputContainer' => '{{content}}'],
			'templateVars' => [
				'label'=>__da('Endorsement'),
				'class' => 'col-sm-12 tinymce',
			]
		])
	)
    .
    $this->Html->div('row row-padding',
        $this->Form->control('image',[
            'type'=>'hidden',
            'templateVars' => [
                'label'=>__da('Image'),
                'class' => 'col-sm-3',
            ]
        ])
    )
    .
    $this->Html->div('row',
        $this->Form->control('home',[
            'type' => 'checkbox',
            'label'=>__da('Show on home page'),
            'templateVars' => [
                'label'=>__da('Show on home page'),
                'class' => 'col-sm-6 checkbox-block',
            ]
        ])
    )
	.
	$this->element('index/save_actions')
	.
	$this->Form->end()
);
?>