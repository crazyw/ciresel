<?php
$this->Paginator->setTemplates([
    'number'       => '<a href="{{url}}">{{text}}</a>',
    'current'      => '<span>{{text}}</span>',
    'prevActive'   => '<a class="pager-prev" href="{{url}}"><i class="icon-caret-left"></i></a>',
    'nextActive'   => '<a class="pager-next" href="{{url}}"><i class="icon-caret-right"></i></a>',
    'nextDisabled' => '',
    'prevDisabled' => '',
    'ellipsis'=> '<a>...</a>'
]);
?>

<div class="action-pager">
    <?= $this->Paginator->prev() ?>
    <?= $this->Paginator->numbers(['first' => 1, 'last' => 1, 'modulus' => 4]) ?>
    <?= $this->Paginator->next() ?>
</div>

<div class="action-info"><?= $this->Paginator->counter(['format' => 'range']) ?></div>

