<div class="content-block">
    <table class="data-table articles-list">

        <?php echo $this->element('index/table_head', ['fields' => $fields, 'model_name' => $model_name] ) ?>

        <?php echo $this->element('index/table_body', ['items' => $items, 'fields' => $fields, 'model_name' => $model_name] ) ?>

    </table>

    <?php echo $this->element('index/footer_actions') ?>

</div>