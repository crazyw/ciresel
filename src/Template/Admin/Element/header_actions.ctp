<div class="content-controls clearfix">
    <div class="content-search">
        <input id="search" type="search" placeholder="<?php echo __da('Search'); ?>" value="<?php echo isset($this->request->query['search']) ? $this->request->query['search'] : '' ?>">
    </div>
    <div class="table-length">
        <div class="styled-select action icon-caret-down">
            <select class="dropdown per-page">
                <option value="10" <?php echo $per_page == 10 ? 'selected' : '' ?>>10</option>
                <option value="25" <?php echo $per_page == 25 ? 'selected' : '' ?>>25</option>
                <option value="50" <?php echo $per_page == 50 ? 'selected' : '' ?>>50</option>
                <option value="100" <?php echo $per_page == 100 ? 'selected' : '' ?>>100</option>
            </select>
        </div>
    </div>

    <div class="content-grid">
<!--        <a class="current" href="#"><i class="icon-th-list"></i></a>-->
<!--        <a href="#"><i class="icon-th-large"></i></a>-->
    </div>
</div>
