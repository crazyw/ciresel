<?php
	if(!isset($header)) {
		$header = true;
	}
?>
<header class="content-header clearfix">
	<!-- page title is generated from js  -->
	<h2 class="page-title"></h2>

	<?php
	if (empty($itemPage) && CONTROLLER_NAME != 'orders') {
		echo $this->element('index/add_button');
	} elseif ( ! empty($langSwitcher)) {
		echo $this->element('fields/lang_switcher');
	}
	?>

</header>

<?php
if (!empty($itemPage)) {

} else {
	if($header) {
		echo $this->element('../Admin/Element/header_actions');
	}
}
?>

<?php //echo $this->Flash->render() ?>
