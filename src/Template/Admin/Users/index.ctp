<?php
echo $this->element('header');

echo $this->element('index/table',[
    'model_name' => MODEL_NAME,
    'items' => ${Cake\Utility\Inflector::variable(CONTROLLER_NAME)},
    'fields' => [
        [
            'checkboxes' => true,
        ],
//        [
//            'name' => 'id',
//            'view_name' => '#',
//            'class' => 'table-author',
//        ],
        [
            'name' => 'full_name',
            'view_name' => 'Full name',
            'is_link' => true,
        ],
        [
            'name' => 'name',
            'view_name' => 'Role',
            'model' => 'Roles',
            'association' => [
                'target'=>'role',
                'controller_name'=>'roles'
            ],
            'is_link' => true,
        ],
        [
            'name' => 'username',
            'view_name' => 'Email (username)',
            'class' => 'table-author',
        ],
        [
            'name' => 'created',
            'view_name' => 'Created',
            'class' => 'width-200',
        ],
        [
            'actions' => ['edit', 'delete'],
        ],
    ],
]);

?>



