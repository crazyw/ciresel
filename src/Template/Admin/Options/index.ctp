<?php

echo $this->element('header');

echo $this->element('index/table',[
    'model_name' => MODEL_NAME,
    'items' => ${Cake\Utility\Inflector::variable(CONTROLLER_NAME)},
    'fields' => [
        [
            'checkboxes' => true,
        ],
        [
            'name' => 'title',
            'view_name' => 'title',
            'is_link' => true,
            'i18n' => true
        ],
        [
            'name' => 'type',
            'view_name' => 'Type',
            'is_link' => true,
            'options' => dev_conf('Config.optionType')
        ],
        [
            'actions' => ['edit', 'delete', 'publish'],
        ],
    ]
]);
?>



