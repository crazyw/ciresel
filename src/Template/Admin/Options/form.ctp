<?php
	echo $this->element('header', ['itemPage' => true,'langSwitcher'=>true]);
	echo $this->Html->div('content-block',
		$this->Form->create($page, ['class' => "form-page"])
		.
		$this->Form->control('id',['type' => 'hidden'])
		.
		$this->Html->div('row',
			$this->Form->control('title',[
				'lang'=>true,
				'label'=>false,
				'templates' => ['inputContainer' => '{{content}}'],
				'templateVars' => [
					'label'=>__da('Title'),
					'class' => 'col-sm-12',
				]
			])
		)
		.
        $this->Html->div('row',
            $this->Form->control('type', [
                'label'        => __da('Type'),
                'class' => 'select2-search',
                'options'      => dev_conf('Config.optionType'),
                'templateVars' => [
                    'template'=>'select2',
                    'class' => 'col-sm-12',
                    'divclass' => 'styled-select-select2'
                ]
            ])
        )
        .
        $this->Html->div('row',
            $this->Form->control('help',[
                'lang'=>true,
                'type' => 'textarea',
                'label'=>false,
                'templates' => ['inputContainer' => '{{content}}'],
                'templateVars' => [
                    'label'=>__da('Help text'),
                    'class' => 'col-sm-12',
                ]
            ])
        )
		.
		$this->element('index/save_actions')
		.
		$this->Form->end()
	);
?>
