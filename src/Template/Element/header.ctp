<header class="header">
    <a href="<?php echo $this->Url->build(['controller'=>'pages','action'=>'display','language'=>LANG])?>">Home</a>
    <ul id="lang-switcher">
        <?php foreach (lang() as $id => $lang){
            if($lang->site == 0) continue;?>
            <li><a href="<?php echo $this->Url->build([
                    'controller'=>CONTROLLER_NAME,
                    'action'=>ACTION_NAME,
                    'language'=>$lang->code,
                    'slug'=> isset($translatedSlugs[$lang->code]) ? $translatedSlugs[$lang->code] : false
                ]); ?>">
                    <?php echo $lang->title;?></a>
            </li>
        <?php }?>
    </ul>

        <?php echo nested_recursive_menu_home($menu,$this,true); ?>


</header>
