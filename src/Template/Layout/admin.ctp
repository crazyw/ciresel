<!DOCTYPE html>
<html lang="en" class="<?php echo conf('Maintenance.status') == 1 ? 'maintenance ' : '' ?>
<?php echo
(in_array(MODEL_NAME,dev_conf('Config.galeryModels')) || in_array(MODEL_NAME,dev_conf('Config.fileModels'))) && in_array(ACTION_NAME,['add','edit']) ?
    'three-columns ' : 'two-columns '
?>
<?php echo isset($_COOKIE['User_snp']) && (int)$_COOKIE['User_snp'] == 0 ? ' minimized' : ''; ?>">
<?php echo $this->element('head') ?>

<body>
<div class="wrapper">
    <?php echo $this->Flash->render('admin') ?>

    <?php echo $this->element('maintenance') ?>

    <?php echo $this->cell('Navigation'); ?>

    <!-- content -->
    <div class="content clearfix">

        <?php echo $this->fetch('content') ?>
        <?php if(in_array(ACTION_NAME,['add','edit'])){
            echo $this->element('fields/image');
            echo $this->element('fields/file');
        } ?>
    </div>
    <!-- End content -->

    <?php echo $this->element('footer') ?>
    <?php if(in_array(ACTION_NAME,['add','edit'])){?>
        <script>
          $(function () {
            $('.sluggable input[type=text]').keyup(function (e) {
              item = this;
              if($('input[data-field-origin=slug][data-lang='+$(this).attr('data-lang')+']').prop('disabled')) {
                $.ajax({
                  type: "POST",
                  dataType: "json",
                  url: '/admin/' + Controller_Name + '/generateSlug',
                  data: $('.form-page').serialize() + '&target=' + $(this).attr('data-field-origin') + '&target_name=' + $(this).attr('name') + '&lang=' + $(this).attr('lang')
                }).done(function (data) {
                  $('#translations-rom-slug').parent().removeClass('has-error');
                  if (data.success == true) {
                    if ($(item).attr('data-field-origin') != 'slug') {
                        <?php foreach (lang() as $lang):?>
                      $('#translations-<?php echo $lang->code;?>-slug').val(data.slug_<?php echo $lang->code;?>);
                        <?php endforeach;?>
                    }
                  } else {
                    $('#translations-rom-slug').parent().addClass('has-error');
                  }
                });
              }
            });
            $('.slug-checker').click(function () {
              if($(this).prop('checked')){
                $(this).parents('div.col-block').find('input[type=text]').prop('disabled',false);
              }else{
                $('.sluggable input[type=text]').keyup()
                $(this).parents('div.col-block').find('input[type=text]').prop('disabled',true);
              }
            })
          });
        </script>
    <?php } ?>
</div>
</body>
</html>
