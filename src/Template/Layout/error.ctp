<!DOCTYPE html>
<html class="page-404">
	<head>
		<?= $this->Html->charset() ?>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no">
    <meta name="theme-color" content="#6C38A0">

		<?php echo $this->element('meta');?>

    <?php echo $this->fetch('before-css'); ?>
    <?php echo $this->Html->css('/css/final.css?v='.filemtime('css/final.css')); ?>
    <?php echo $this->fetch('after-css'); ?>

		<?php echo $this->element('favicons'); ?>

	</head>
	<body class="<?php echo isset($bodyClass) ? $bodyClass : '' ?>">
		<div class="site-wrapper">
			<div class="site-wrapper-inner">
				<div class="cover-container">
					<a class="logo-404" href="/"><img src="/img/logo.svg" alt=""></a>
					<h1><?= __ds('Page not found') ?></h1>
					<a class="btn btn-medium btn-navy-gradient" href="/"><span><?= __ds('Go to homepage') ?></span></a>
				</div>
			</div>
		</div>
	</body>
</html>
