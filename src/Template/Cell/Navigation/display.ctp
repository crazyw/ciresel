<!-- aside left -->
<div class="navigation">
	<header class="navigation-header clearfix">
		<a class="navigation-home" href="<?php echo $this->Url->build('/'); ?>" target="_blank" title="Home page"><i class="icon-home"></i></a>
		<a class="minimize-menu <?php echo isset($_COOKIE['User_snp']) && (int)$_COOKIE['User_snp'] == 0 ? 'min' : ''; ?>" href="#">
			<i class="<?php echo isset($_COOKIE['User_snp']) && (int)$_COOKIE['User_snp'] == 0 ? 'icon-long-arrow-right' : 'icon-long-arrow-left'; ?>" title="Minimize menu"></i>
		</a>
	</header>

	<div class="hide-scroll">
		<ul class="navigation-menu">
			<?php
			if ($superUser || ! empty($permissions) ) {

				$controllerName = strtolower($this->request->params['controller']);
				foreach($menu as $key => $val) {
					if ( isset($permissions[$key]) || $superUser ) {

						$currentClass = '';
						if($controllerName == $val['link']) {
							$currentClass = ' class="current"';
							/*
							if (isset($val['submenu'])) {
								$currentClass = ' class="current open-menu-item"';
							}*/

						}

						?>
						<li<?php echo $currentClass ?> >
							<a href="<?php echo $this->Url->build('/admin/' . $val['link']); ?>">
								<span class="nav-icon" title="<?php echo $val['name'] ?>"><i class="icon-<?php echo $val['icon'] ?>"></i></span>
								<?php if ( isset($val['submenu']) ) { ?>
									<span class="nav-caret"><i class="icon-caret-down"></i></span>
								<?php } ?>
								<span class="nav-text"><?php echo __da($val['name']); ?></span>
							</a>
							<?php if ( isset($val['submenu']) ) { ?>
								<ul class="navigation-sub-menu">
									<?php foreach($val['submenu'] as $k => $el) {
										if ( isset($permissions[$k]) || $superUser ) {
											$submenuCurrentClass = '';
											if($controllerName == $el['link'] || (isset($el['controller_name']) && $el['controller_name'] == $controllerName)) {
												$submenuCurrentClass = ' class="current"';
											}
											?>
											<li<?php echo $submenuCurrentClass ?> >
												<a href="<?php echo $this->Url->build('/admin/' . $el['link']); ?>">
													<?php echo __da($el['name']); ?>
												</a>
											</li>
										<?php } ?>
									<?php } ?>
								</ul>
							<?php } ?>
						</li>
					<?php }  ?>
				<?php } ?>
			<?php }  ?>
		</ul>
	</div>

	<footer class="navigation-footer clearfix">
		<div class="project-logo">
			<img class="logo-icon" src="/img/favicons/logo.jpg" alt="" >
		</div>

		<div class="navigation-exit">
			<a href="<?php echo $this->Url->build('/admin/users/logout'); ?>"><i class="icon-power-off"></i></a>
		</div>
		<div class="name-user"><p><?php echo __da('Hello') ?> <?php echo $userData['full_name'] ?></p></div>

	</footer>


</div>
<!-- End aside left -->
<script>
	$(document).ready(function() {
		// generate page title

		// first level (main menu)
		var separator = ' <i class="icon-angle-right"></i> ';
		var title1 = $(".navigation-menu > li.current > a > span.nav-text").text();

		// second level (submenu)
		var $activeSubmenu = $("ul.navigation-sub-menu > li.current");
		if ($activeSubmenu.length) {
			$activeSubmenu.parents('li').addClass('open-menu-item');
		}
		var title10 = $activeSubmenu.parent().prev('a').find('span.nav-text').text();
		var title2 = $activeSubmenu.text();

		// if submenu active - show submenu title, else - main menu
		var title = title2.length ? title10 + separator + title2 : title1;

		<?php if (ACTION_NAME == 'add'){ ?>
		title += separator + "<?= __da('Add') ?>";
		<?php } elseif (ACTION_NAME == 'edit'){ ?>
		title += separator + "<?= __da('Edit') ?>";
		<?php } ?>

		$("h2.page-title").prepend(title);



		$('.navigation-header .minimize-menu').click(function(e){
			if ($(this).hasClass('min')) {
				$(this).removeClass('min');
				$('html').removeClass('minimized');
				$(this).find('.icon-long-arrow-right').addClass('icon-long-arrow-left').removeClass('icon-long-arrow-right');
								document.cookie = "User_snp=1; path=/; expires=" + new Date(new Date().getTime() + 60*60*24*120*1000).toUTCString();
			} else {
				$(this).addClass('min');
				$('html').addClass('minimized');
				$('.open-menu-item .nav-caret').click();
				$(this).find('.icon-long-arrow-left').addClass('icon-long-arrow-right').removeClass('icon-long-arrow-left');
								document.cookie = "User_snp=0; path=/; expires=" + new Date(new Date().getTime() + 60*60*24*120*1000).toUTCString();
			}
			e.preventDefault();
		});

	});
</script>
