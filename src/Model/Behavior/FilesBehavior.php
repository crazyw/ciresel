<?php
namespace App\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\ORM\Table;

use Cake\Event\Event;
use Cake\Datasource\EntityInterface;
use Cake\Utility\Inflector;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\ORM\Query;
use App\Controller\Component\AmigoFileComponent;
use Cake\Filesystem\Folder;

/**
 * Image behavior
 */
class FilesBehavior extends Behavior
{

	/**
	 * Default configuration.
	 *
	 * @var array
	 */
	protected $_defaultConfig = [
		'fields' => [],
	];


	public function afterSave(Event $event, EntityInterface $entity)
	{
		$module_id = dev_conf('Modules')[$event->subject()->getAlias()]->id;
		if($module_id && $entity->file_list) {
			$fileTable = \Cake\ORM\TableRegistry::get('Files');
			$i = 0;
			foreach ($entity->file_list as $random_name_folder => $file_name){
				if(!is_int($random_name_folder)) {
					$file = $fileTable->newEntity();
					$file->name = $file_name;
					$file->module_id = $module_id;
					$file->foreign_key = $entity->id;
					$file->title = $entity->file_title[$random_name_folder];
					$file->position = $i;
					$save = $fileTable->save($file);
					if($save){
						$source = FILE_PATH.strtolower($event->subject()->getAlias()).DS.'files'.DS.FILE_PATH_TMP.DS.$random_name_folder;
						$destination = FILE_PATH.strtolower($event->subject()->getAlias()).DS.'files'.DS.$file->id;
						$dir = new Folder();
						$copy = $dir->copy([
							'to' => $destination,
							'from' => $source,
							'mode' => 0777,
						]);
						if($copy){
							$deleteTmpFolder = new Folder($source);
							$deleteTmpFolder->delete();
						}
					}
				}else{
					$file = $fileTable->get($random_name_folder);
					$file->position = $i;
					$save = $fileTable->save($file);
				}
				$i++;
			}
		}
	}

}
