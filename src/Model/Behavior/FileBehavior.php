<?php
namespace App\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\ORM\Table;

use Cake\Event\Event;
use Cake\Datasource\EntityInterface;
use Cake\Utility\Inflector;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\ORM\Query;
use App\Controller\Component\AmigoFileComponent;
use Cake\Filesystem\Folder;

/**
 * Image behavior
 */
class FileBehavior extends Behavior
{

	/**
	 * Default configuration.
	 *
	 * @var array
	 */
	protected $_defaultConfig = [
		'fields' => [],
	];


	public function afterSave(Event $event, EntityInterface $entity)
	{
		if($entity->isNew()) {
			$alias = $event->subject()->getAlias();
			foreach (dev_conf('Config.'.$alias.'.fileFields') as $fileField => $config) {
//				$fileField = $config['input'];
				if(isset($entity->{$fileField}) && $entity->{$fileField}){
					$random_name_folder = $entity->random[$fileField];
//                    $source = IMAGE_PATH.strtolower($event->subject()->getAlias()).DS.IMAGE_PATH_TMP.DS.$entity->{$imageField};
					$source = FILE_PATH.strtolower($alias).DS.FILE_PATH_TMP.DS.$random_name_folder;
					$destination = FILE_PATH.strtolower($alias).DS.$entity->id;
					$dir = new Folder();
					$copy = $dir->copy([
						'to' => $destination,
						'from' => $source,
						'mode' => 0777,
					]);
					if($copy){
						$deleteTmpFolder = new Folder($source);
						$deleteTmpFolder->delete();
					}
				}
			}
		}
	}

}
