<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\ORM\Behavior\Translate\TranslateTrait;


/**
 * Page Entity
 *
 * @property int $id
 * @property string $title
 * @property \Cake\I18n\Time $created
 * @property int $position
 */
class AbstractEntity extends Entity
{

	public function getImageItem($prefix = '',$folder_preview = false,$model = MODEL_NAME){
		if($prefix == 'gallery'){
			$id = $this->id;
			$name = $this->name;
			$dir_prefix = '/'.$prefix;
		}else{
			$id = $this->id;
			$name = $this->{$prefix};
			$dir_prefix = '';
		}

		$image_config = dev_conf('Config.singleImage.'.$model.'.'.$prefix);

		$imageParent = '/'.IMAGE_PATH_WEB.strtolower($model).$dir_prefix.'/'.$id.'/'.$name;
		$folder_preview  = !$folder_preview ? $image_config['preview']['folder'] : $folder_preview;
		if(isset($image_config['preview']['folder'])){
			if(isset($image_config['thumbs'][$image_config['preview']['folder']])){
				$image = '/'.IMAGE_PATH_WEB.strtolower($model).$dir_prefix.'/'.$id.'/'.$folder_preview.'/'.$name;
			}elseif (isset($image_config['crop']['thumbs'][$image_config['preview']['folder']])){
				$image = '/'.IMAGE_PATH_WEB.strtolower($model).$dir_prefix.'/'.$id.'/'. $folder_preview. '/'.
						\App\Controller\Component\AmigoFileComponent::getCropPrefix($image_config) . \App\Controller\Component\AmigoFileComponent::replaceExtensionToPng($name);
			}
		}
		if(isset($image) && is_file(WWW_ROOT.$image)){
			return $image;		
		}else{
			if(is_file(WWW_ROOT.IMAGE_PATH_WEB.strtolower($model).$dir_prefix.'/'.$id.'/'.$folder_preview.'/'.$name)){
				return '/'.IMAGE_PATH_WEB.strtolower($model).$dir_prefix.'/'.$id.'/'.$folder_preview.'/'.$name;
			}elseif(is_file(WWW_ROOT.$imageParent)){
				return $imageParent;
			}else{
				return false;
			}
		}
	}

}
