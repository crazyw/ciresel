<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\Event\Event;
use Cake\Network\Request;
use Cake\Routing\Router;
use Cake\Core\Configure;

/**
 * amigo Model
 *
 */

class AbstractTable extends \Cake\ORM\Table
{
    public $search_field = [];
    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
    	parent::initialize($config);

    	//behavior for language
		if(Configure::read('Config.'.$config['alias'].'.translate')) {
			$this->addBehavior('Translate', Configure::read('Config.'.$config['alias'].'.translate'));
		}

        // behavior for slug
		if(\Cake\Core\Configure::read('Modules')) {
			if (in_array($config['alias'], array_keys(\Cake\Core\Configure::read('Modules'))) && \Cake\Core\Configure::read('Modules')[$config['alias']]['sluggable'] == 1) {
				$this->addBehavior('Sluggable');
				$this->hasMany('Slugs')
                ->setForeignKey('foreign_key')
                ->setDependent(false);
            }
        }

        // behavior for search
		if(Configure::read('Config.'.$config['alias'].'.searchFields') && isset($_GET['search'])){
			$this->addBehavior('Search',[
					'fields'=>Configure::read('Config.'.$config['alias'].'.searchFields')]
			);
		}elseif(isset($_GET['search'])){
			$this->addBehavior('Search',[
					'fields'=>['title']
				]
			);
		}

        //behavior for single image
        $this->addBehavior('Images');

        //behavior for gallery
		if(\Cake\Core\Configure::read('Config.galeryModels.'.$config['alias'])){
			$this->addBehavior('Gallery');
			$module_id = \Cake\Core\Configure::read('Modules')[$config['alias']]->id;
			$this->hasMany('Images')
				->setForeignKey('foreign_key')
				->setConditions(['module_id' => $module_id])
				->setDependent(true);

			// call when U need just one image from gallery
			$this->hasOne('FirstImage', [
				'className' => 'Images',
				'foreignKey' => 'foreign_key',
				'strategy' => 'select',
				'sort' => ['FirstImage.position' => 'DESC'],
				'conditions' => function ($e, $query) {
					$query->order('FirstImage.position DESC')->group('FirstImage.foreign_key');
					return [];
				}
			]);
		}

		//behavior for single file
		if(\Cake\Core\Configure::read('Config.'.$config['alias'].'.fileFields')) {
			$this->addBehavior('File');
		}

		//behavior for multiple file
		if(\Cake\Core\Configure::read('Config.fileModels.'.$config['alias'])){
			$this->addBehavior('Files');
			$module_id = \Cake\Core\Configure::read('Modules')[$config['alias']]->id;
			$this->hasMany('Files')
				->setForeignKey('foreign_key')
				->setConditions(['module_id' => $module_id])
				->setDependent(true);
		}
    }

    public function beforeSave(Event $event)
    {
        $entity = $event->getData('entity');
        // convert date fields value in mysql format
        foreach (dev_conf('Config.dateFields') as $df) {
            if ( isset($entity->{$df}) && ! empty($entity->{$df}) ) {
                $entity->{$df} = date("Y-m-d H:i:s", strtotime($entity->{$df}));
            }
        }
    }

    public function findActive(Query $query,$options = []){
    	if(isset($options['name'])){
			return $query->where([$options['name'].'.published'=>1,$this->getAlias().'.id > ' => 0]);
		}else{
			return $query->where(['published'=>1,$this->getAlias().'.id > ' => 0]);
		}
	}

	public function findOrder(Query $query){
		return $query->order(['position'=>'ASC']);
	}

}