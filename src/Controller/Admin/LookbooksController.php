<?php
namespace App\Controller\Admin;

use Cake\Network\Exception\NotFoundException;

class LookbooksController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        parent::index();
        $this->set('title_for_layout', 'Lookbooks');
        $this->loadModel('Lookbooks');
        $this->paginate['order']['Lookbooks.id'] = 'desc';
        $lookbooks= $this->paginate($this->Lookbooks);
        $this->set(compact('lookbooks'));
    }

    public function add() {
        $this->set('title_for_layout', 'Lookbooks : Add');
        $this->loadModel('Lookbooks');
        $page = $this->Lookbooks->newEntity($this->request->data);

        $this->set(compact('page'));
        $this->formValidation(false);
    }

    public function edit($id = NULL) {
        $this->loadModel('Lookbooks');

        $page = $this->Lookbooks
            ->find('translations')
            ->where(['Lookbooks.id'=>$id])
            ->first();

        $this->set('title_for_layout', 'Lookbooks : '.$page->full_name);
        if (empty($page)) {
            throw new NotFoundException('Could not find that page.');
        } else {
            $this->set(compact('page'));
        }
        $this->formValidation(false);
    }

}
