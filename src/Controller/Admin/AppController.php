<?php
namespace App\Controller\Admin;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Network\Exception\UnauthorizedException;

class AppController extends AmigoController {


	public function initialize()
	{
        parent::initialize();
        $this->loadComponent('UserLog');
//		$this->UserLog->allow(['add','edit','delete','formValidation','publish']);
		$this->UserLog->allow(['delete','formValidation']);
		$this->UserLog->newLog();
	}

    function beforeFilter(Event $event) {
        parent::beforeFilter($event);
    }

    public function beforeRender(Event $event)
    {
        parent::beforeRender($event);
        if($this->request->is('ajax')){
            $this->viewBuilder()->layout('ajax');
        }
    }

}
