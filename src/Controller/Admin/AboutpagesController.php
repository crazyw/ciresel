<?php
namespace App\Controller\Admin;

use Cake\Network\Exception\NotFoundException;

class AboutpagesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        parent::index();
        $this->set('title_for_layout', 'Aboutpages');
        $this->loadModel('Aboutpages');
        $this->paginate['order']['Aboutpages.position'] = 'asc';
        $this->paginate['order']['Aboutpages.id'] = 'desc';
        $this->paginate['limit'] = 100;
        $aboutpages = $this->paginate($this->Aboutpages);
        $this->set(compact('aboutpages'));
    }

    public function add() {
        $this->set('title_for_layout', 'Endorsements : Add');
        $this->loadModel('Endorsements');
        $page = $this->Endorsements->newEntity($this->request->data);

        $this->set(compact('page'));
        $this->formValidation(false);
    }

    public function edit($id = NULL) {
        $this->loadModel('Aboutpages');
        $page = $this->Aboutpages->find('translations')->where(['Aboutpages.id'=>$id])->first();
        $this->set('title_for_layout', 'Aboutpages : '.$page->title);
        if (empty($page)) {
            throw new NotFoundException('Could not find that page.');
        } else {
            $this->set(compact('page'));
        }
        $this->formValidation(false);
    }
}
