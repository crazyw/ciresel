<?php
namespace App\Controller\Admin;

use Cake\Network\Exception\NotFoundException;
/**
 * NewsCategories Controller
 *
 * @property \App\Model\Table\NewsCategoriesTable $NewsCategories
 */
class FaqController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
	public function index() {
		parent::index();
		$this->set('title_for_layout', 'Faq');
		$this->loadModel('Faq');
		$this->paginate['order']['Faq.position'] = 'asc';
		$this->paginate['order']['Faq.id'] = 'desc';
		$faq = $this->paginate($this->Faq);
		$this->set(compact('faq'));
	}

	public function add() {
		$this->set('title_for_layout', 'Faq : Add');
		$this->loadModel('Faq');
		$page = $this->Faq->newEntity($this->request->data);

		$this->set(compact('page'));
		$this->formValidation(false);
	}

	public function edit($id = NULL) {
		$this->loadModel('Faq');
		$page = $this->Faq->find('translations')->where(['Faq.id'=>$id])->first();
		$this->set('title_for_layout', 'Faq : '.$page->title);
		if (empty($page)) {
			throw new NotFoundException('Could not find that page.');
		} else {
			$this->set(compact('page'));
		}
		$this->formValidation(false);
	}
}
