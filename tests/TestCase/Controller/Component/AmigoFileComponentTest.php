<?php
namespace App\Test\TestCase\Controller\Component;

use App\Controller\Component\AmigoFileComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\Component\AmigoFileComponent Test Case
 */
class AmigoFileComponentTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Controller\Component\AmigoFileComponent
     */
    public $AmigoFile;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->AmigoFile = new AmigoFileComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AmigoFile);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
