<?php
namespace App\Test\TestCase\Controller\Admin;

use App\Controller\Admin\SpecializationsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Admin\SpecializationsController Test Case
 */
class SpecializationsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.specializations',
        'app.companies',
        'app.slugs',
        'app.languages',
        'app.translations',
        'app.companies_meta_description_translation',
        'app.companies_meta_title_translation',
        'app.companies_meta_keywords_translation',
        'app.i18n',
        'app.companies_specializations',
        'app.members',
        'app.members_specializations'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
