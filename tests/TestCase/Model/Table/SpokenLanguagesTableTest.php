<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SpokenLanguagesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SpokenLanguagesTable Test Case
 */
class SpokenLanguagesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SpokenLanguagesTable
     */
    public $SpokenLanguages;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.spoken_languages',
        'app.members',
        'app.languages',
        'app.translations',
        'app.specializations',
        'app.specializations_title_translation',
        'app.i18n',
        'app.companies',
        'app.slugs',
        'app.companies_meta_description_translation',
        'app.companies_meta_title_translation',
        'app.companies_meta_keywords_translation',
        'app.companies_specializations',
        'app.members_specializations',
        'app.companies_members',
        'app.members_spoken_languages'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SpokenLanguages') ? [] : ['className' => 'App\Model\Table\SpokenLanguagesTable'];
        $this->SpokenLanguages = TableRegistry::get('SpokenLanguages', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SpokenLanguages);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
