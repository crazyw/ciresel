<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserLogTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserLogTable Test Case
 */
class UserLogTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UserLogTable
     */
    public $UserLog;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.user_log',
        'app.users',
        'app.roles',
        'app.permissions',
        'app.actions_modules',
        'app.actions',
        'app.modules',
        'app.items'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UserLog') ? [] : ['className' => 'App\Model\Table\UserLogTable'];
        $this->UserLog = TableRegistry::get('UserLog', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserLog);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
