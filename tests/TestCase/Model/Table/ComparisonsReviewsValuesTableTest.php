<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ComparisonsReviewsValuesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ComparisonsReviewsValuesTable Test Case
 */
class ComparisonsReviewsValuesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ComparisonsReviewsValuesTable
     */
    public $ComparisonsReviewsValues;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.comparisons_reviews_values',
        'app.comparison_reviews'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ComparisonsReviewsValues') ? [] : ['className' => 'App\Model\Table\ComparisonsReviewsValuesTable'];
        $this->ComparisonsReviewsValues = TableRegistry::get('ComparisonsReviewsValues', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ComparisonsReviewsValues);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
