<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ComparisonsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ComparisonsTable Test Case
 */
class ComparisonsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ComparisonsTable
     */
    public $Comparisons;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.comparisons',
        'app.categories',
        'app.categories_title_translation',
        'app.categories_meta_description_translation',
        'app.categories_meta_title_translation',
        'app.categories_meta_keywords_translation',
        'app.categories_content_translation',
        'app.i18n',
        'app.slugs',
        'app.languages',
        'app.translations'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Comparisons') ? [] : ['className' => 'App\Model\Table\ComparisonsTable'];
        $this->Comparisons = TableRegistry::get('Comparisons', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Comparisons);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
