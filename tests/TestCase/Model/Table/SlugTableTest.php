<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SlugTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SlugTable Test Case
 */
class SlugTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SlugTable
     */
    public $Slug;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.slug',
        'app.modules',
        'app.languages',
        'app.translations'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Slug') ? [] : ['className' => 'App\Model\Table\SlugTable'];
        $this->Slug = TableRegistry::get('Slug', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Slug);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
