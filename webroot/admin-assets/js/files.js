$(function () {
    var singleFile;
    singleFile = true;
    InitUploadFile();
    InitFileEdit();
    InitFilesSortable();
});
function InitUploadFile() {
    $('.file_upload').click(function (e) {
        e.preventDefault();
        if($(this).attr('data-multiple') == '1'){
            singleFile = false;
        }else{
            singleFile = true;
        }
        $(this).prev().click();
    });

    // check and upload files
    $('.file_upload_trigger').unbind().on('change', function () {
        //define local variable
        item = this;
        formEl = singleFile ? $("form.form-page") : $("form.gallery-form");
        console.log('init');
        // Validate Picture
        if (!window.FileReader || !this.files) {
            // Old Browsers
            console.log('Old Browser');
        }
        else if (!this.files[0]) {
            // No Pictures
            console.log('No file');
        }
        else
        {
            targetName = $(item).parent().find('.file_name').attr('name');
            //Send image data in uploadImage
            formEl.ajaxSubmit({
                url: '/admin/'+Controller_Name+'/uploadFile?name='+targetName,
                dataType: "json",
                success: function (response) {
                    $.each(response, function( i, v ) {
                        if (v.success) {
                            if (singleFile) {
                                $(item).parent().find('.original_name').val(v.filename);
                                if ($(item).parent().find('.file_name').val() == '') {
                                    $(item).parent().find('.file_name').val(v.filename);
                                }
                                //                            $(item).parent().find('.random-file').val(response.uniqid);
                                $(item).parent().addClass('with-file');
                                $(item).parent().find('.download-file').attr('href', '/' + v.path + v.filename)
                            } else {
                                template = _.template($('#filesTmp').html());
                                $(item).parent().append(template({
                                    filename: v.filename,
                                    random: v.random
                                }));
                                $('#files-random').val(v.uniqid);
                                InitFileEdit();
                                InitFilesSortable();
                            }
                        } else {
                            alert(v.error);
                        }
                    });
                }
            });
        }
    });
}

function InitFileEdit() {
    $('.file-title').click(function(e){
        if (!$(this).hasClass('edit')) {
            var val = $(this).text();
            $(this).parent().addClass('edit');
            // autosize(document.querySelectorAll('.translate-item-edit .form-control'));
            $('.file-title-edit .form-control').focus();
        }
        e.preventDefault();
    });
}
function deleteFile(item,id,name) {
    if (!confirm('Are you sure?')) {
        return false;
    }
    $.ajax('/admin/'+Controller_Name+'/deleteFile', {
        method: "POST",
        data: {
            id:id,
            name:name,
        },
        dataType: "json",
        success: function (response) {
            if(name == 'files'){
                $(item).parent().remove();
            }else{
                $(item).parent().removeClass('with-file');
                $(item).parent().find('input').val('');
                $(item).parent().find('.random-file').val(response.name);
            }
        },
        error: function () {
        }
    });
}
function InitFilesSortable() {
    $('#dropdown2').sortable();
}