<?php
namespace Users\Model\Table;


use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

use Cake\Auth\DefaultPasswordHasher;
use Cake\Event\Event;
use Cake\Datasource\EntityInterface;

/**
 * Menus Model
 *
 * @method \App\Model\Entity\Menu get($primaryKey, $options = [])
 * @method \App\Model\Entity\Menu newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Menu[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Menu|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Menu patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Menu[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Menu findOrCreate($search, callable $callback = null, $options = [])
 */
class UsersTable extends \App\Model\Table\AbstractTable
{

	/**
	 * Initialize method
	 *
	 * @param array $config The configuration for the Table.
	 * @return void
	 */
	public function initialize(array $config)
	{
		parent::initialize($config);

		$this->setTable('users');
		$this->setDisplayField('id');
		$this->setPrimaryKey('id');
		$this->belongsTo('Roles');

		$this->hasMany('UserSettings', [
			'foreignKey' => 'user_id'
		]);

		$this->addBehavior('Timestamp');
	}

	public function beforeSave(Event $event)
	{
	    $entity = $event->getData('entity');;

		if ( ! empty($entity->password_input) ) {
			$hasher = new DefaultPasswordHasher();
			$entity->password = $hasher->hash($entity->password_input);
		}

		return true;
	}
	/**
	 * Default validation rules.
	 *
	 * @param \Cake\Validation\Validator $validator Validator instance.
	 * @return \Cake\Validation\Validator
	 */
	public function validationDefault(Validator $validator)
	{
//        $validator
//            ->integer('id')
//            ->allowEmpty('id', 'create');
//
//        $validator->add('email', 'valid', [
//            'rule' => 'email',
//            'message' => 'Invalid email'
//        ]);
//        $validator
//            ->requirePresence('full_name', 'create')
//            ->notEmpty('full_name');

//        $validator
//            ->requirePresence('username', 'create')
//            ->notEmpty('username')
//            ->add('username', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);
//
//        $validator
//            ->requirePresence('password', 'create')
//            ->notEmpty('password');
//
//        $validator
//            ->allowEmpty('role');
//
//        $validator
//            ->boolean('published')
//            ->allowEmpty('published');

		return $validator;
	}

	/**
	 * Returns a rules checker object that will be used for validating
	 * application integrity.
	 *
	 * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
	 * @return \Cake\ORM\RulesChecker
	 */
//    public function buildRules(RulesChecker $rules)
//    {
//        $rules->add($rules->isUnique(['username']));
//
//        return $rules;
//    }
}
