<?php
use Migrations\AbstractMigration;

class AddModulesAndRoles extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
	public function up()
	{
		$this->query("INSERT INTO `modules` (`id`, `name`, `sluggable`)
VALUES
	(NULL , 'Modules', 0), (NULL, 'Roles', 0);
");
	}
}
