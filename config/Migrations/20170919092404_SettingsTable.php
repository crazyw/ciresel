<?php
use Migrations\AbstractMigration;

class SettingsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
	public function up()
	{
		$this->query("CREATE TABLE `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `col_group` varchar(255) DEFAULT NULL,
  `col_key` varchar(255) NOT NULL,
  `value` text,
  PRIMARY KEY (`id`),
  KEY `key` (`col_group`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
INSERT INTO `settings` (`id`, `col_group`, `col_key`, `value`)
VALUES
	(1, 'Config', 'projectName', 'Amigo'),
	(2, 'Meta', 'description', ''),
	(3, 'Default', 'title', ''),
	(4, 'Maintenance', 'status', '0'),
	(5, 'Maintenance', 'nolimit', '0'),
	(6, 'Maintenance', 'ips', ''),
	(7, 'Maintenance', 'message_rom', 'Alert'),
	(8, 'Languages', 'eng', '0'),
	(9, 'Languages', 'rus', '0'),
	(10, 'LanguagesFront', 'eng', '0'),
	(11, 'LanguagesFront', 'rus', '1'),
	(12, 'Language', 'admin', '1'),
	(13, 'Language', 'front', '1');
");
	}
}
