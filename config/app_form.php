<?php
$form['inputContainer']    = '<div class="col-block {{class}}">{{content}}</div>';
$form['inputContainerError'] = '<div class="col-block {{class}}">{{content}}</div>';

$form['input']             = '<input class="form-control {{inputclass}}" type="{{type}}" name="{{name}}"{{attrs}}/>';
$form['label']             = '<label {{attrs}}>{{text}} <span>{{lang}}</span></label>';

$form['inputSubmit']       = '<input class="btn {{class}}" type="{{type}}"{{attrs}}/>';
$form['submitContainer']   = '<div class="col-block {{divclass}}">{{content}}</div>';

$form['select']            = '<div class="styled-select form-control icon-caret-down">
                                <select name="{{name}}"{{attrs}}>{{content}}</select>
                              </div>';

$form['select2']           = '<div class="{{divclass}}">
                                <select name="{{name}}"{{attrs}}>{{content}}</select>
                              </div>';
$form['select2-tag']       = '<div class="{{divclass}}">
                                <select name="{{name}}[]"{{attrs}} multiple="multiple">{{content}}</select>
                              </div>';


$form['buttonContainer']   = '<div class="col-block {{divclass}}">{{content}}</div>';

$form['checkbox']          = '<input type="checkbox" class="checkbox" name="{{name}}" value="{{value}}"{{attrs}}>';

$form['textarea']          = '<textarea name="{{name}}"{{attrs}}>{{value}}</textarea>';

$form['language']          = '<div class="col-block {{class}}">
								<label>{{text}}{{slug}} <span>{{lang}}</span></label>
								<input class="form-control {{inputclass}}" type="{{type}}" name="{{name}}"{{attrs}}/>
							</div>';

$form['languageTextarea']   = '<div class="col-block {{class}}">
								 <label {{attrs}}>{{text}} <span>{{lang}}</span></label>
								 <textarea class="form-control" name="{{name}}"{{attrs}}>{{value}}</textarea>
							  </div>';

$form['datetimepicker']    = '<div class="col-block {{class}}">
								 <label {{attrs}}>{{text}}</label>
								 <input type="hidden" name="{{name}}"{{attrs}}/>
								 <input class="form-control datetimepicker" type="{{type}}" value="{{convert_data}}"/>
							  </div>';

$form['single_image']    =   '<div class="col-block {{class}}">
                                    <input type="{{type}}" name="{{name}}"{{attrs}}/>
                                    <input type="file" name="photo[{{name}}]" style="display: none;"  class="image-file">
                                    {{random}}
                                    <h3>{{text}}</h3>
                                    <div class="images-gallery clearfix">
                                        <div class="gallery-item">
                                            <button class="btnAddItem"></button>
                                        </div>
                                    </div>
                               </div>';

$form['single_image_with_image']    = '<div class="col-block {{class}}">
                                            <input type="{{type}}" name="{{name}}"{{attrs}}/>
                                            <input type="file" name="photo[{{name}}]" style="display: none;" class="image-file">
                                            <h3>{{text}}</h3>
                                            <div class="images-gallery clearfix">
                                                <div class="gallery-item">
                                                    <div class="image"><img src="{{image_name}}" alt=""></div>
                                                    <button class="btnDeleteItem"><i class="icon-trash-o"></i></button>
                                                </div>
                                            </div>
                                      </div>';

$form['single_file']               = '<div class="upload-file {{actions}}">
											{{random}}
											<input name="{{name}}" type="text" class="form-control file_name" value="{{inputValue}}">
											{{input}}
											<input name="file_upload[{{name}}]" type="file" class="hidden file_upload_trigger">
											<button class="btn btn-medium btn-default file_upload">Browse</button>
									
											<a class="download-file" title="download file" href="{{fileLink}}" target="_blank"><i class="icon-download"></i></a>
											<a class="remove-file" title="remove file" onclick="deleteFile(this,\'{{id}}\',\'{{name}}\')"><i class="icon-trash-o"></i></a>
										</div>';
$form['formEnd']               = '</form>';
return $form;
